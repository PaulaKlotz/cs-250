#ifndef _PROCESSOR
#define _PROCESSOR

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
using namespace std;

#include "Job.hpp"
#include "Queue.hpp"

class Processor
{

public:
    void FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile );
    void RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile );
};

void Processor::FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile )
{
	ofstream output(logFile);
	output << "First Come, First Served" << endl;

	int cycles = 0;

	while (jobQueue.Size() > 0)
	{
		output << "Current Job ID: " << jobQueue.Front()->id << endl;
		output << "Cycle: " << cycles << endl;
		output << "Remaining: " << jobQueue.Front()->fcfs_timeRemaining << endl;
		jobQueue.Front()->Work(FCFS);

		if (jobQueue.Front()->fcfs_done)
		{
			jobQueue.Front()->SetFinishTime(cycles, FCFS);
			jobQueue.Pop();

		}

		cycles++;
	}

	output << "FCFS Results" << endl;
	for (unsigned int i = 0; i < allJobs.size(); i++)
	{
		output << allJobs[i].id << "\t" << allJobs[i].fcfs_finishTime << endl;
	}

	int totTime = 0;
	output << "Summary: " << endl;
	for (unsigned int i = 0; i < allJobs.size(); i++)
	{
		int avgTime = allJobs[i].fcfs_finishTime / allJobs.size();
		output << "Average Time: " << avgTime << endl;
		totTime += allJobs[i].fcfs_finishTime;
		output << "Total Processing Time: " << totTime << endl;
	}

	output.close();
}

void Processor::RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile )
{
	ofstream output(logFile);
	output << "Round Robin" << endl;

	int cycles = 0;
	int timer = 0;

	while (jobQueue.Size() > 0)
	{
		if (timer == timePerProcess)
		{
			jobQueue.Front()->rr_timesInterrupted++;
			jobQueue.Push(jobQueue.Front());
			jobQueue.Pop();
			timer = 0;
		}

		output << "Job ID: " << jobQueue.Front()->id << endl;
		output << "Cycle: " << cycles << endl;
		output << "Time Remaining: " << jobQueue.Front()->rr_timeRemaining << endl;
		jobQueue.Front()->Work(RR);

		if (jobQueue.Front()->rr_done)
		{
			jobQueue.Front()->SetFinishTime(cycles, RR);
			jobQueue.Pop();
		

		}

		cycles++;
		timer++;
	}

	output << "RR Results: " << endl;
	for (unsigned int i = 0; i < allJobs.size(); i++)
	{
		output << allJobs[i].id << "\t" << allJobs[i].rr_finishTime <<
			"\t" << allJobs[i].rr_timesInterrupted << endl;
	}

	int totTime = 0;
	output << "Summary: " << endl;
	for (unsigned int i = 0; i < allJobs.size(); i++)
	{
		int avgTime = allJobs[i].rr_finishTime / allJobs.size();
		output << "Average Time: " << avgTime << endl;
		totTime += allJobs[i].rr_finishTime;
		output << "Total Processing Time: " << totTime << endl;
		output << "Intervals: " << timePerProcess << endl;
	}

	output.close();
}


#endif
