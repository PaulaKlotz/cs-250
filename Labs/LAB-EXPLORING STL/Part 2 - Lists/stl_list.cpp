// Lab - Standard Template Library - Part 2 - Lists
// FIRSTNAME, LASTNAME

#include <iostream>
#include <list>
#include <string>
using namespace std;

void DisplayList(list<string>& states)
{
	for (list<string>::iterator it = states.begin();
		it != states.end(); it++)
	{
		cout << *it << "\t";
	}
	cout << endl;
}

int main()
{
	list<string> states;
	bool done = false;
	while (!done) {

		cout << "Main Menu: ";
		cout << "List Size: " << states.size() << endl;
		cout << "1. Add new states to front of list "
			<< "2. Add new state to back of list "
			<< "3. Pop state from front of list "
			<< "4. Pop state from end of list "
			<< "5. Continue " << endl;

		int choice;
		cin >> choice;

		if (choice == 1) {
			string frontState;
			cout << "Which state would you like to add to the front? " << endl;
			cin >> frontState;
			states.push_front(frontState);

		}
		else if (choice == 2) {
			string backState;
			cout << "Which state would you like to add to the back: " << endl;
			cin >> backState;
			states.push_back(backState);
		}
		else if (choice == 3) {
			states.pop_front();
		}
		else if (choice == 4) {
			states.pop_back();
		}
		else if (choice == 5) {
			for (list<string>::iterator it = states.begin(); it != states.end(); it++)
			{
				done = true;
			}
		}
	}

		cout << "Normal List: " << endl;
		DisplayList(states);

		cout << "Reverse List: " << endl;
		states.reverse();
		DisplayList(states);

		cout << "Sort List: " << endl;
		states.sort();
		DisplayList(states);

		cout << "Reverse-Sort List: " << endl;
		states.reverse();
		DisplayList(states);

	cin.ignore();
    cin.get();
    return 0;
}


