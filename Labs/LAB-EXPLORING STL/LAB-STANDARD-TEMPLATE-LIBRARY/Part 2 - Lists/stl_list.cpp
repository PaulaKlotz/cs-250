// Lab - Standard Template Library - Part 3 - Queues
// Paula Klotz

#include <iostream>
#include <string>
#include <queue>
using namespace std;

int main()
{
	queue<float> transactions;

	bool done = false;

	while (!done) {

		cout << "Menu Selection: " << endl;
		cout << "1. Transaction Amount: "
			"2. Continue: " << endl;

		int choice;
		cin >> choice;

		if (choice == 1) {
			float amount;
			cout << "What is the amount? " << endl;
			cin >> amount;
			transactions.push(amount);
		}
		else if (choice == 2) {
			done = true;
		}
	}

	float balance = 0;

	while (transactions.size() > 0) {

		cout << "Display front-most transaction amount: " << endl;
		cout << transactions.front() << endl;

		cout << "-----------------------------------------" << endl;
		cout << "Add the front-most amount to the balance variable: " << endl;
		balance += transactions.front();
		cout << balance;

		cout << "-----------------------------------------" << endl;
		cout << "Pop the front of the queue: " << endl;
		transactions.pop();
	}

	cout << "Balance: " << balance << endl;


	cin.ignore();
	cin.get();
	return 0;
}
