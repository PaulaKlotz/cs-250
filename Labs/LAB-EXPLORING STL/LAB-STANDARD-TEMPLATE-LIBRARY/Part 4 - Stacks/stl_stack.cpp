// Lab - Standard Template Library - Part 4 - Stacks
// Paula Klotz

#include <iostream>
#include <string>
#include <stack>
using namespace std;

int main()
{
	stack<string> letters;
	bool done = false;
	string choice;

	cout << "Enter next letter of word, "
		<< "or type UNDO to undo last letter, "
		<< "or type DONE if you are finished." << endl;
		
	while (!done) {

		cin >> choice;


	if (choice <= "A" || choice > "Z") {

		letters.push(choice);
	}
	else if (choice == "UNDO") {
		
		letters.pop();
		
	}
	else if (choice == "DONE") {

		done = true;
	}
    }

	cout << "Final Word: " << endl;

	while (!letters.empty()) {

		cout << letters.top();
		letters.pop();
	}

	
	cin.ignore();
    cin.get();
    return 0;

}
