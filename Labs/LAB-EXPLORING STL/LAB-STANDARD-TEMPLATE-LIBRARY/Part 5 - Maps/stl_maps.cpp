// Lab - Standard Template Library - Part 5 - Maps
// Paula Klotz

#include <iostream>
#include <string>
#include <map>
using namespace std;

int main()
{
	map<char, string> colors;
	bool done = false;

	colors['r'] = "FF0000";
	colors['g'] = "00FF00";
	colors['b'] = "0000FF";
	colors['c'] = "00FFFF";
	colors['m'] = "FF00FF";
	colors['y'] = "FFFF00";

	while (!done) {

		char choice;
		cout << "Enter a color letter, or 'q' to stop: ";
		cin >> choice;

		if (choice == 'q') {

			cout << "Goodbye" << endl;
			done = true;
		}
		else {

			cout << "Hex: " << colors[choice] << endl;
			cout << endl;
		}
	}
	
	
	cin.ignore();
    cin.get();
    return 0;
}
