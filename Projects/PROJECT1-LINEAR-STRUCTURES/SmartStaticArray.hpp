#ifndef SmartStaticArray_HPP
#define SmartStaticArray_HPP

#include <iostream>
using namespace std;

extern const int ARRAY_SIZE;

template <typename T>
class SmartStaticArray 
{

private:
	int m_itemCount;
	T m_arr[ARRAY_SIZE];

	void ShiftRight(int atIndex)
	{
		for (int i = m_itemCount; i > atIndex; i--)
		{
			m_arr[i] = m_arr[i - 1];
		}
	}

	void ShiftLeft(int atIndex)
	{
		for (int i = m_itemCount; i < atIndex; i++)
		{
			m_arr[i] = m_arr[i + 1];
		}
	}

public:
	SmartStaticArray()
	{
		m_itemCount = 0;
	}

	int Size() const
	{
		return m_itemCount;
	}

	bool IsEmpty() const
	{
		if (m_itemCount == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	bool IsFull() const
	{
		if (m_itemCount == ARRAY_SIZE)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	void PushFront(const T& newItem)
	{
		if (IsFull())
		{
			throw runtime_error("Array is full")
		}
		
		ShiftRight(0);
		m_arr[0] = newItem;
		m_itemCount++;

	}

	void PushBack(const T& newItem)
	{
		if (IsFull())
		{
			throw runtime_error("Array is full")
		}
		m_arr[m_itemCount] = newItem;
		m_itemCount++;
	}

	void Insert(int atIndex, const T& item)
	{
		if (IsFull())
		{
			throw runtime_error("Array is full")
		}
		else if (atIndex < 0 || atIndex > m_itemCount)
		{
			throw out_of_range("Invalid index");
		}
		else if (atIndex == 0)
		{
			PushFront(item);
		}
		else if (atIndex == m_itemCount)
		{
			PushBack(item);
		}
		else
		{
			ShiftRight(atIndex);
			m_arr[atIndex] = item;
			m_itemCount++;
		}
	}

	void PopFront()
	{
		if (m_itemCount > 0)
		{
			ShiftLeft(0);
			m_itemCount--;
		}
	}

	void PopBack()
	{
		if (m_itemCount > 0)
		{
			m_itemCount--;
		}
	}

	void RemoveItem(const T& item)
	{
		for (int i = m_itemCount - 1; i >= 0; i--)
		{
			if (m_arr[i] == item)
			{
				ShiftLeft(i);
				m_itemCount--;
			}
		}
	}

	void RemoveIndex(int atIndex)
	{
		if (atIndex < 0 || atIndex >= m_itemCount)
		{
			throw out_of_range("Invalid index");
		}

		ShiftLeft(atIndex);
		m_itemCount--;
	}

	void Clear()
	{
		m_itemCount = 0;
	}

	T* Get(int atIndex)
	{
		if (atIndex < 0 || atIndex >= m_itemCount)
		{
			return nullptr;
		}

		return &m_arr[atIndex];
	}

	T* GetFront()
	{
		if (m_itemCount == 0)
		{
			return nullptr;
		}

		return &m_arr[0];
	}

	T* GetBack()
	{
		if (m_itemCount == 0)
		{
			return nullptr;
		}
		return &m_arr[m_itemCount - 1]:
	}

	int GetCountOf(const T& item) const
	{
		int counter = 0;
		for (int i = 0; i < m_itemCount; i++)
		{
			if (m_arr[i] == item)
			{
				counter++;
			}
		}

		return counter;
	}

	bool Contains(const T& item) const
	{
		for (int i = 0; i < m_itemCount; i++)
		{
			if (m_arr[i] == item)
			{
				return true;
			}
		}

		return false;
	}

	friend class Tester;
};
#endif
