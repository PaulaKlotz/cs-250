#include "Tester.hpp"

void Tester::RunTests()
{
    Test_IsEmpty();
    Test_IsFull();
    Test_Size();
    Test_GetCountOf();
    Test_Contains();

    Test_PushFront();
    Test_PushBack();

    Test_Get();
    Test_GetFront();
    Test_GetBack();

    Test_PopFront();
    Test_PopBack();
    Test_Clear();

    Test_ShiftRight();
    Test_ShiftLeft();

    Test_Remove();
    Test_Insert();
}

void Tester::DrawLine()
{
    cout << endl;
    for ( int i = 0; i < 80; i++ )
    {
        cout << "-";
    }
    cout << endl;
}

void Tester::Test_Init()
{
    DrawLine();
    cout << "TEST: Test_Init" << endl;

    // Put tests here
}

void Tester::Test_ShiftRight()
{
    DrawLine();
    cout << "TEST: Test_ShiftRight" << endl;

    // Put tests here
}

void Tester::Test_ShiftLeft()
{
    DrawLine();
    cout << "TEST: Test_ShiftLeft" << endl;

    // Put tests here
}

void Tester::Test_Size()
{
    DrawLine();
    cout << "TEST: Test_Size" << endl;

    {   // Test begin
        cout << endl << "Test 1" << endl;
        List<int> testList;
        int expectedSize = 0;
        int actualSize = testList.Size();

        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;

        if ( actualSize == expectedSize )
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }   // Test end

    {   // Test begin
        cout << endl << "Test 2" << endl;
        List<int> testList;

        testList.PushBack( 1 );

        int expectedSize = 1;
        int actualSize = testList.Size();

        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;

        if ( actualSize == expectedSize )
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }   // Test end
}

void Tester::Test_IsEmpty()
{
	DrawLine();
	cout << "TEST: Test_IsEmpty" << endl;

	// Put tests here

	//Test 1
	List<int> testlist;

	bool expectedValue = true;
	bool actualValue = testlist.IsEmpty();

	cout << "Created empty list; IsEmpty()should be true" << endl;
	if (expectedValue == actualValue)
	{
		cout << "Pass" << endl;
	}
	else {
		cout << "Fail" << endl;
	}

	//Test 2
	List<int> testlist;

	testlist.PushBack(1);

	bool expectedValue = false;
	bool actualValue = testlist.IsEmpty();

	cout << "Created 1 item in list; IsEmpty()should be false" << endl;
	if (expectedValue == actualValue)
	{
		cout << "Pass" << endl;
	}
	else
	{
		cout << "Fail" << endl;
	}
}



void Tester::Test_IsFull()
{
    DrawLine();
    cout << "TEST: Test_IsFull" << endl;

    // Put tests here
}

void Tester::Test_PushFront()
{
    DrawLine();
    cout << "TEST: Test_PushFront" << endl;

    // Put tests here
}

void Tester::Test_PushBack()
{
	DrawLine();
	cout << "TEST: Test_PushBack" << endl;

	// Put tests here

	//Test 1
	cout << endl; << "Test 1" << endl;

	List<string> testlist;

	bool expectedValue = true;
	bool actualValue = testlist.PushBack("A");

	cout << "Create a list, PushBack 1 item, should be successful" < , endl;

	if (expectedValue == actualValue)
	{
		cout << "Pass" << endl;
	}
	else
	{
		cout << "Fail" << endl;
	}
}

// Test 2
cout << endl; << "Test 2" << endl;

List<string> testlist;
testlist.PushBAck("B");

int expectedValue = 1;
int acutalValue = testlist.Size();

cout << "Create a list, PushBack 1 item, Size() should be 1" << endl;

If(ExpectedValue == actualValue)
{
	cout << "Pass" << endl;
}
else
{
	cout << "Fail" << endl;
}

// Test 3
cout << endl; << "Test 3" << endl;
cout << "Prerequisite: Get()" << endl;

List<string> testlist;
testlist.PushBack("A");
testlist.PushBack("B");
testlist.PushBack("C");

string expectedValues[3] = {
	"A",
	"B",
	"C",
};
string* actualValues[3] = {
	testlist.Get(0),
	testlist.Get(1),
	testlist.Get(2)
};

if (actualValue[0] == nullptr || actualValue[1] == nullptr, actualValue[2] == nullptr)
{
	cout << "Got nullptrs; avoid segfault. Returning" << endl;
	return;
}

if (*actualValues[0] == expectedValues[0] &&
	*actualValues[1] == expectedValues[1] &&
	*actualValues[2] == expectedValues[2])
{
	cout << "Pass" << endl;
}
else
{
	cout << "Fail" << endl;
}

void Tester::Test_PopFront()
{
    DrawLine();
    cout << "TEST: Test_PopFront" << endl;

    // Put tests here
}

void Tester::Test_PopBack()
{
    DrawLine();
    cout << "TEST: Test_PopBack" << endl;

    // Put tests here
}

void Tester::Test_Clear()
{
    DrawLine();
    cout << "TEST: Test_Clear" << endl;

    // Put tests here
}

void Tester::Test_Get()
{
    DrawLine();
    cout << "TEST: Test_Get" << endl;

    // Put tests here
}

void Tester::Test_GetFront()
{
    DrawLine();
    cout << "TEST: Test_GetFront" << endl;

    // Put tests here
}

void Tester::Test_GetBack()
{
    DrawLine();
    cout << "TEST: Test_GetBack" << endl;

    // Put tests here
}

void Tester::Test_GetCountOf()
{
    DrawLine();
    cout << "TEST: Test_GetCountOf" << endl;

    // Put tests here
}

void Tester::Test_Contains()
{
    DrawLine();
    cout << "TEST: Test_Contains" << endl;

    // Put tests here
}

void Tester::Test_Remove()
{
    DrawLine();
    cout << "TEST: Test_Remove" << endl;

    // Put tests here
}

void Tester::Test_Insert()
{
    DrawLine();
    cout << "TEST: Test_Insert" << endl;

    // Put tests here
}
