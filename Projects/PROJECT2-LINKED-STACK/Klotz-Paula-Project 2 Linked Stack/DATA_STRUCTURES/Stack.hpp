#ifndef _STACK_HPP
#define _STACK_HPP

#include "Node.hpp"

template <typename T>
class LinkedStack
{
    public:
		LinkedStack();
		void Push(const T& newData) noexcept;
		void Pop();
		T& Top();
		int Size() noexcept;

private:
	Node<T>* m_ptrFirst;
	Node<T>* m_ptrLast;
	int m_itemCount;
};


	template <typename T>
	LinkedStack<T>::LinkedStack()
    {
		m_itemCount = 0;
		m_ptrFirst = nullptr;
		m_ptrLast = nullptr;
    }

	template <typename T>
	void LinkedStack<T>::Push(const T& newData) noexcept
    {
		if (m_ptrFirst == nullptr)
		{
			Node<T>* t = new Node<T>;
			t->data = newData;
			t->ptrNext = nullptr;
			m_ptrFirst = t;
			m_ptrLast = t;
		}
		else if (m_ptrFirst != nullptr)
		{
			Node<T>* t = new Node<T>;
			t->data = newData;
			t->ptrNext = m_ptrFirst;
			m_ptrFirst = t;
		}

		m_itemCount++;
    }

	template <typename T>
    T& LinkedStack<T>::Top()
    {
		if (m_ptrFirst == nullptr)
		{
			//throw CourseNotFound("Stack is empty");
		}
		return m_ptrFirst->data;
    }

	template <typename T>
    void LinkedStack<T>::Pop()
    {
		if (m_ptrFirst == nullptr)
		{
			cout << "Is empty" << endl;
		}
		else if (m_ptrFirst == m_ptrLast)
		{
			//cout << "Last value is popped" << m_ptrFirst->data << endl;
			delete m_ptrFirst;
			m_ptrFirst = nullptr;
			m_ptrLast = nullptr;
		}
		else
		{
			Node<T>* t = new Node<T>;
			t = m_ptrFirst;
			m_ptrFirst = m_ptrFirst->ptrNext;
			delete t;
		}

		m_itemCount--;
    }

	template <typename T>
    int LinkedStack<T>::Size() noexcept
    {
       return m_itemCount;    // placeholder
    }

   

#endif
