#include "CourseCatalog.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

#include "UTILITIES/Menu.hpp"
#include "EXCEPTIONS/CourseNotFoundException.hpp"
#include "DATA_STRUCTURES/Stack.hpp" // LinkedStack

CourseCatalog::CourseCatalog()
{
    LoadCourses();
}

void CourseCatalog::LoadCourses() // done
{
    Menu::Header( "LOADING COURSES" );

    ifstream input( "courses.txt" );

    if ( !input.is_open() )
    {
        cout << "Error opening input text file, courses.txt" << endl;
        return;
    }

    string label, courseCode, courseName, prerequisite;
    Course newCourse;

    while ( input >> label )
    {
        if ( label == "COURSE" )
        {
            if ( newCourse.name != "" )
            {
                m_courses.PushBack( newCourse );
                newCourse.Clear();
            }

            input >> newCourse.code >> newCourse.name;
        }
        else if ( label == "PREREQ" )
        {
            input >> newCourse.prereq;
        }
    }

    input.close();

    cout << " * " << m_courses.Size() << " courses loaded" << endl << endl;
}

void CourseCatalog::ViewCourses() noexcept
{
    Menu::Header( "VIEW COURSES" );
	
	for (int i = 0; i < m_courses.Size(); i++)
	{

		cout << i << "\t" << m_courses[i].code << "\t"
			<< m_courses[i].name 
		    << "\t" << m_courses[i].prereq << endl;
		
	}
}

Course CourseCatalog::FindCourse( const string& code )
{
	for (int i = 0; i < m_courses.Size(); i++)
	{
		Course lookat = m_courses[i];
		if (lookat.code == code)
		{
			return m_courses[i];
		}
	}
	
			throw CourseNotFound("Course not found");
}

void CourseCatalog::ViewPrereqs() noexcept 
{
    Menu::Header( "GET PREREQS" );

	string input;
	Course current;

	cout << "What is the course code? ";
	cin >> input;

	try
	{
		current = FindCourse(input);
	}
	catch (CourseNotFound& e)
	{
		cout << e.what() << endl;
	}

	LinkedStack<Course> prereqs;

	while(current.prereq != " ")
	{
		prereqs.Push(current);

		try
		{
			current = FindCourse(current.prereq);
		}
		catch (CourseNotFound& e)
		{
			cout << e.what() << endl;
			break;
		}
	}

	while (prereqs.Size() > 0)
	{
		cout << prereqs.Top().code << "/t" <<
			 prereqs.Top().name << endl;
		prereqs.Pop();
	}
}

void CourseCatalog::Run()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "MAIN MENU" );

        int choice = Menu::ShowIntMenuWithPrompt( { "View all courses", "Get course prerequisites", "Exit" } );

        switch( choice )
        {
            case 1:
                ViewCourses();
            break;

            case 2:
                ViewPrereqs();
            break;

            case 3:
                done = true;
            break;
        }
    }
}
